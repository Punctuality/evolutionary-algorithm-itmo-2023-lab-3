package lab3;

import lab3.evoalgo.FitnessFunction;
import lab3.evoalgo.MyCrossover;
import lab3.evoalgo.MyFactory;
import lab3.evoalgo.MyMutation;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.GenerationCount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MyAlg {

    public static void main(String[] args) {
        int dimension = 20; // dimension of problem
        int populationSize = 100; // size of population
        int generations = 100; // number of generations

        Random random = new Random(42); // random

        CandidateFactory<double[]> factory = new MyFactory(dimension, -5, 5); // generation of solutions

        ArrayList<EvolutionaryOperator<double[]>> operators = new ArrayList<>();
        operators.add(new MyCrossover(true)); // Crossover
        operators.add(new MyMutation(-5, 5, 1, 1, true)); // Mutation
        EvolutionPipeline<double[]> pipeline = new EvolutionPipeline<>(operators);

        SelectionStrategy<Object> selection = new RouletteWheelSelection(); // Selection operator

        FitnessEvaluator<double[]> evaluator = new FitnessFunction(dimension); // Fitness function

        EvolutionEngine<double[]> algorithm = new SteadyStateEvolutionEngine<>(
                factory, pipeline, evaluator, selection, populationSize, false, random);

        algorithm.addEvolutionObserver(populationData -> {
            double bestFit = populationData.getBestCandidateFitness();
            System.out.println("Generation " + populationData.getGenerationNumber() + ": " + bestFit);
            System.out.println("\tBest solution = " + Arrays.toString(populationData.getBestCandidate()));
            System.out.println("\tPop size = " + populationData.getPopulationSize());
        });

        TerminationCondition terminate = new GenerationCount(generations);
        algorithm.evolve(populationSize, 1, terminate);
    }
}
