package lab3.utils;

import java.util.Random;

public class RandomUtils {

        public static double randomDoubleWithOffset(double offset, Random random) {
            return randomDoubleInRange(-offset, offset, random);
        }

       public static double randomDoubleInRange(double minBound, double maxBound, Random random) {
           return random.nextDouble() * (maxBound - minBound) + minBound;
       }

       public static double keepInRange(double minBound, double maxBound, double value) {
           return Math.max(minBound, Math.min(maxBound, value));
       }
}
