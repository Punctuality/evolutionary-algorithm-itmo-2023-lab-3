package lab3.evoalgo;

import org.uncommons.watchmaker.framework.operators.AbstractCrossover;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyCrossover extends AbstractCrossover<double[]> {

    /**
     * Since I don't know internals of framework yet, I've added this option to preserve previous inputs untouched
     */
    private final boolean isImmutable;

    public MyCrossover(boolean isImmutable) {
        super(1);
        this.isImmutable = isImmutable;
    }

    private void swapElement(double[] a1, double[] a2, int idx) {
        double tmp = a1[idx];
        a1[idx] = a2[idx];
        a2[idx] = tmp;
    }

    protected List<double[]> mate(double[] p1, double[] p2, int numberOfCrossoverPoints, Random random) {
        if (p1.length != p2.length)
            throw new IllegalStateException("Input candidates cannot have different dimensions on crossover");

        ArrayList<double[]> crossOver = new ArrayList<>(2);;
        if (isImmutable) {
            crossOver.add(p1.clone());
            crossOver.add(p2.clone());
        } else {
            crossOver.add(p1);
            crossOver.add(p2);
        }
        for (int i = 0; i < numberOfCrossoverPoints; i++) {
            int swapPoint = random.nextInt(p1.length);
            swapElement(crossOver.get(0), crossOver.get(1), swapPoint);
        }

        return crossOver;
    }
}
