package lab3.evoalgo;

import lab3.utils.RandomUtils;
import org.uncommons.watchmaker.framework.EvolutionaryOperator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MyMutation implements EvolutionaryOperator<double[]> {

    final boolean isImmutable;
    final double minBound;
    final double maxBound;
    final double maxDiff;
    final double changeProb;

    public MyMutation(double minBound, double maxBound, double maxDiff, double changeProb, boolean isImmutable) {
        this.isImmutable = isImmutable;
        this.minBound = minBound;
        this.maxBound = maxBound;
        this.maxDiff = maxDiff;
        this.changeProb = changeProb;
    }

    private double[] mutate(double[] candidate, Random random) {
        if (isImmutable)
            candidate = candidate.clone();

        for (int i = 0; i < candidate.length; i++) {
            if (random.nextDouble() <= changeProb) {
                candidate[i] += RandomUtils.randomDoubleWithOffset(this.maxDiff, random);
                candidate[i] = RandomUtils.keepInRange(this.minBound, this.maxBound, candidate[i]);
            }
        }

        return candidate;
    }


    public List<double[]> apply(List<double[]> population, Random random) {
        ArrayList<double[]> mutated = new ArrayList<>(population.size());

        for (double[] candidate : population) {
            mutated.add(mutate(candidate, random));
        }

        return mutated;
    }
}
