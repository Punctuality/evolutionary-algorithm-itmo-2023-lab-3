package lab3.evoalgo;

import lab3.utils.RandomUtils;
import org.uncommons.watchmaker.framework.factories.AbstractCandidateFactory;

import java.util.Random;

public class MyFactory extends AbstractCandidateFactory<double[]> {

    private final int dimension;
    private final double minBound;
    private final double maxBound;

    public MyFactory(int dimension, double minBound, double maxBound) {
        this.dimension = dimension;
        this.minBound = minBound;
        this.maxBound = maxBound;
    }

    public double[] generateRandomCandidate(Random random) {
        double[] solution = new double[this.dimension];

        for (int idx = 0; idx < this.dimension; idx++)
            solution[idx] = RandomUtils.randomDoubleInRange(this.minBound, this.maxBound, random);

        return solution;
    }
}

