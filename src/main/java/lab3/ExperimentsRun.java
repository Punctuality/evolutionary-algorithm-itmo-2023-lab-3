package lab3;

import com.google.common.collect.ImmutableList;
import lab3.eval.ExperimentResult;
import lab3.eval.ExperimentUnit;
import lab3.eval.GridEvaluator;
import org.javatuples.Pair;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Function;

class ExperimentsRun {

    final static List<String> paramNames = ImmutableList.of(
            "Max mutation diff", "Mutation Prob", "Dimension", "Population Size", "Generations", "Repeats"
    );
    final static List<Double> maxDiffs = ImmutableList.of(1.4);
    final static List<Double> mutationProbs = ImmutableList.of(0.01);
    final static List<Integer> dimensions = ImmutableList.of(2, 5, 10, 20, 50, 100);
    final static List<Integer> populationSizes = ImmutableList.of(10, 50, 100);
    final static List<Integer> generations = ImmutableList.of(100, 500, 1000, 10000);
    final static List<Integer> repeatCount = ImmutableList.of(10);
    final static Function<List<?>, ExperimentUnit> experimentCompiler = objects -> new ExperimentUnit(
            (int) objects.get(2),
            (int) objects.get(3),
            (int) objects.get(4),
            (int) objects.get(5),
            (double) objects.get(0),
            (double) objects.get(1)
    );


    final static List<String> resultNames = ImmutableList.of("Best fitness", "First generation of best", "Mean time (ms)");
    final static Function<ExperimentResult[], List<String>> rowLogger = results -> {
        Pair<Double, Integer> bestOverAll =
                Arrays.stream(results)
                        .map(r -> new Pair<>(r.bestResult(), r.firstAchievedIter()))
                        .max((o1, o2) -> {
                            int c1 = o2.getValue0().compareTo(o1.getValue0());
                            if (c1 == 0) {
                                return o1.getValue1().compareTo(o2.getValue1());
                            } else {
                                return c1;
                            }
                        }).get();

        Double meanTime = ((double) Arrays.stream(results).mapToLong(ExperimentResult::evalTime).sum()) / (double) results.length;

        return ImmutableList.of(bestOverAll.getValue0().toString(), bestOverAll.getValue1().toString(), meanTime.toString());
    };

    public static void main(String[] args) {
        File outputFile = new File("tmp/results.csv");
        if (!outputFile.exists()) {
            try {
                outputFile.createNewFile();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }

        GridEvaluator<ExperimentResult[], ExperimentUnit> evaluator = new GridEvaluator<>(
                outputFile,
                experimentCompiler,
                rowLogger,
                paramNames,
                resultNames,
                maxDiffs,
                mutationProbs,
                dimensions,
                populationSizes,
                generations,
                repeatCount
        );

        try (ForkJoinPool executePool = new ForkJoinPool(Runtime.getRuntime().availableProcessors())) {
            System.out.println("Started executing Grid experiment run");
            executePool.invoke(evaluator);
            System.out.println("Finished execution of all tasks");
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
