package lab3.eval;

import java.util.*;

public record ExperimentResult(double bestResult, double[] bestCandidate, int firstAchievedIter, long evalTime) implements Comparable<ExperimentResult> {
    @Override
    public int compareTo(ExperimentResult that) {
        return Objects.compare(this, that,
                Comparator.comparing(ExperimentResult::bestResult)
                        .thenComparing(ExperimentResult::firstAchievedIter)
                        .thenComparing(ExperimentResult::evalTime)
        );
    }
}
