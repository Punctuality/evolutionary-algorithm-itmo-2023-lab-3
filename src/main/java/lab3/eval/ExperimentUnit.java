package lab3.eval;

import lab3.evoalgo.FitnessFunction;
import lab3.evoalgo.MyCrossover;
import lab3.evoalgo.MyFactory;
import lab3.evoalgo.MyMutation;
import org.uncommons.watchmaker.framework.*;
import org.uncommons.watchmaker.framework.operators.EvolutionPipeline;
import org.uncommons.watchmaker.framework.selection.RouletteWheelSelection;
import org.uncommons.watchmaker.framework.termination.GenerationCount;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class ExperimentUnit extends RecursiveTask<ExperimentResult[]> {
    final int dimension;
    final int populationSize;
    final int generations;
    final Random[] randoms;
    final double maxMutationDiff;
    final double mutationProb;

    public ExperimentUnit(int dimension, int populationSize, int generations, int repeatCount, double maxMutationDiff, double mutationProb) {
        this.dimension = dimension;
        this.populationSize = populationSize;
        this.generations = generations;
        this.randoms = new Random[repeatCount];
        for (int i = 0; i < repeatCount; i++) {
            this.randoms[i] = new Random(i);
        }
        this.mutationProb = mutationProb;
        this.maxMutationDiff = maxMutationDiff;
    }

    RecursiveTask<ExperimentResult> setUpTask(
            CandidateFactory<double[]> factory,
            EvolutionPipeline<double[]> pipeline,
            SelectionStrategy<Object> selection,
            Random random
    ) {
        final FitnessEvaluator<double[]> evaluator = new FitnessFunction(dimension);

        final EvolutionEngine<double[]> algorithm = new SteadyStateEvolutionEngine<>(
                factory,
                pipeline,
                evaluator,
                selection,
                populationSize,
                false,
                random
        );

        return new RecursiveTask<>() {
            @Override
            protected ExperimentResult compute() {

                AtomicReference<Double> bestFit = new AtomicReference<>(Double.MIN_VALUE);
                AtomicInteger firstAchievedIter = new AtomicInteger(0);

                algorithm.addEvolutionObserver(populationData -> {

                    double currentFit = populationData.getBestCandidateFitness();

                    if (bestFit.get() < currentFit) {
                        bestFit.set(currentFit);
                        firstAchievedIter.set(populationData.getGenerationNumber());
                    }
                });

                TerminationCondition terminate = new GenerationCount(generations);

                long startTime = System.currentTimeMillis();
                double[] bestCandidate = algorithm.evolve(populationSize, 1, terminate);
                long endTime = System.currentTimeMillis();

                return new ExperimentResult(bestFit.get(), bestCandidate, firstAchievedIter.get(), endTime - startTime);
            }
        };
    }


    @Override
    protected ExperimentResult[] compute() {
        CandidateFactory<double[]> factory = new MyFactory(dimension, -5, 5);

        ArrayList<EvolutionaryOperator<double[]>> operators = new ArrayList<>();
        operators.add(new MyCrossover(true));
        operators.add(new MyMutation(-5, 5, this.maxMutationDiff, this.mutationProb, true));
        EvolutionPipeline<double[]> pipeline = new EvolutionPipeline<>(operators);

        SelectionStrategy<Object> selection = new RouletteWheelSelection();

        ArrayList<RecursiveTask<ExperimentResult>> tasks = new ArrayList<>(this.randoms.length);

        for (Random random : this.randoms) {
            tasks.add(setUpTask(factory, pipeline, selection, random));
        }

        System.out.printf("Running experiment [dim: %d, populationSize: %d, generations: %d, maxDiff: %.2f, mutationProb: %.2f, repeats: %d]\n",
                this.dimension, this.populationSize, this.generations, this.maxMutationDiff, this.mutationProb, this.randoms.length);
        return ForkJoinTask.invokeAll(tasks).stream().map(ForkJoinTask::join).toArray(ExperimentResult[]::new);
    }
}
